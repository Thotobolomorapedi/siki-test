import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalController} from '@ionic/angular'

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import { NavController } from '@ionic/angular';
import { User } from '../entities/user';
import { LoadingService } from '../services/loading/loading.service';
import { from } from 'rxjs';
import { TermsPage } from '../terms/terms.page';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  signUpFormGroup: FormGroup;
  passwordType: any;
  passwordConfirmType: any
  user: User;
  emailSent = false;
  email: any;
  constructor(
    public loading: LoadingService,
    private fb: FormBuilder,
    private navCtrl: NavController, 
    public modalController: ModalController) { }

  ngOnInit() {
    this.initiForms()
  }
  initiForms() {
    this.signUpFormGroup = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    })
  }
  togglePasswordType() {
    this.passwordType = this.passwordType || 'password';
    this.passwordType = (this.passwordType === 'password') ? 'text' : 'password';
  }
  toggleConfirmPasswordType() {
    this.passwordConfirmType = this.passwordConfirmType || 'password';
    this.passwordConfirmType = (this.passwordConfirmType === 'password') ? 'text' : 'password';
  }
  register() {
    this.loading.present('Creating your account ...')
    this.user = new User();
    firebase.auth().createUserWithEmailAndPassword(this.signUpFormGroup.controls.email.value, this.signUpFormGroup.controls.password.value)
      .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        this.loading.dismiss();
       // this.navCtrl.navigateRoot('registered');
        this.navCtrl.navigateRoot('/registered');
        console.log(user);
        user.sendEmailVerification().then(function () {
          console.log("verification email sent")
        }).catch(function (error) {
          console.log("verification error", error);
        });
        // ...
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        // ..
        this.loading.dismiss();
      });
  }
  get f(): any {
    return this.signUpFormGroup.controls;
  }


  async presentModal() {
    const modal = await this.modalController.create({
      component: TermsPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }



}
