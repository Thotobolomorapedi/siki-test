import { LoadingService } from './../services/loading/loading.service';
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  date: any;
  tab: any = 'All';
  transactionsDB: any = [];
  posts: any;


  constructor(
    private modalController: ModalController,
    public popoverController: PopoverController,
    public loading: LoadingService) {
    this.date = Date.now();
  }

  removePost(post){
    let index =this.posts.indexOf(post);
    if (index > -1) {
      this.posts.splice(index, 1);
    }
  }

  ngOnInit(): void {
    this.getAllTransactions();
  }

  getAllTransactions() {
    var user = firebase.auth().currentUser.uid;
    var rootRef = firebase.database().ref('transactions/' + user);
    this.loading.present("Loading Transactions ...")
    rootRef.get().then(snapshot => {
      if (snapshot.exists()) {
        console.log("s", snapshot.val()); 
        snapshot.forEach(snapshot => {
          this.transactionsDB.push(snapshot.val())
        });

        console.log("gah", this.transactionsDB);
        this.loading.dismiss()
      }
      else {
        console.log("No data available");
      }
    }).catch(function (error) {
      console.error(error);
    });

  }


}
