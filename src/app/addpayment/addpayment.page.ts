import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import firebase from 'firebase/app'
import 'firebase/auth'
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-addpayment',
  templateUrl: './addpayment.page.html',
  styleUrls: ['./addpayment.page.scss'],
})
export class AddpaymentPage implements OnInit {
  paymentMethods: any[];
  smegaPayment: any[];
  paymentMethod: FormGroup;
  constructor(private modalController: ModalController, private router: Router, private fb: FormBuilder, private storage: Storage) { }

  ngOnInit() {
    this.initiForm()
  }
  initiForm() {
    this.paymentMethod = this.fb.group({
      phoneNumber: ['', [Validators.required]]
    })
  }

 

  addPaymentOption() {
    var user = firebase.auth().currentUser;
    this.smegaPayment = [{
      name: "Smega",
      Type: "Wallet",
      number: this.paymentMethod.controls.phoneNumber.value,
      owner: user.displayName ? user.displayName : "Not set",
      expiry: Date.now()
    }]
    this.storage.set('paymentOptions', this.smegaPayment);
    this.storage.set('payerID', this.paymentMethod.controls.phoneNumber.value);
    this.router.navigateByUrl('/tabs/tab2', {replaceUrl:true});
  }

}
