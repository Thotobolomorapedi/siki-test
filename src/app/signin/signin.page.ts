import { LoadingService } from './../services/loading/loading.service';
import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/app'
import 'firebase/auth'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {
  otpSent = false;
  recaptchaVerifier: any;
  verificationId: any;
  loginFormGroup: FormGroup;
  verificationCode: any;
  recaptcha: any;
  phoneNumber: any;

  constructor(
    private router: Router,
    public loading: LoadingService,
    public toastController: ToastController,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible',
      'callback': (response) => {
        console.log("captcha solved");
      },
      'expired-callback': () => {
        console.log("captcha expired");
      }
    });

    this.initForms();

  }
  initForms() {
    this.loginFormGroup = this.fb.group({
      phoneNumber: ['', [Validators.required, Validators.minLength(11)]],
      verificationCode: ['']
    })
  }

  async sendOtp() {
    this.loading.present('Sending OTP ...');

    const appVerifier = this.recaptchaVerifier;
    
    const phoneNumberString = "+" + this.loginFormGroup.controls.phoneNumber.value;
    this.phoneNumber = this.loginFormGroup.controls.phoneNumber.value;

    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then((confirmationResult) => {
        console.log("response", confirmationResult);
        this.otpSent = true;
        this.verificationId = confirmationResult;
        this.presentToast("Verification code sent to " + phoneNumberString, "success");
        this.loading.dismiss();

      })
      .catch(function (error) {
        console.error("SMS not sent", error);
        this.presentToast();
        this.loading.dismiss();
      });

  }


  verifyOtp() {
    this.loading.present('Verifying OTP')
    //   {
    //   message: 'Verifying OTP ...',
    //   duration: 5000
    // })
    this.verificationCode = this.loginFormGroup.controls.verificationCode.value
    this.verificationId.confirm(this.verificationCode)
      .then(function (result) {
        // User signed in succesionisfully.
        console.log("user", result.user);
        if (result) {
          console.log("user in", result.user);
          this.ngZone.run(() => {
            this.router.navigateByUrl('/tabs', { replaceUrl: true });
            this.loadingCtrl.dismiss();
          });
        }

      }),error => {
        console.log("error", error);
        this.presentToast("User couldn't sign in, bad verification code","danger");
        this.loading.dismiss();

      };
  }



  presentToast(message, color) {
    this.toastController.create({
      message: message,
      duration: 2000,
      color: color
    })
      .then(toast => toast.present());
  }


  get f(): any {
    return this.loginFormGroup.controls;
  }
}
