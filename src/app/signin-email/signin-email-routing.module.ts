import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SigninEmailPage } from './signin-email.page';

const routes: Routes = [
  {
    path: '',
    component: SigninEmailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SigninEmailPageRoutingModule {}
