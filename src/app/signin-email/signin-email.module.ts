import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SigninEmailPageRoutingModule } from './signin-email-routing.module';

import { SigninEmailPage } from './signin-email.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SigninEmailPageRoutingModule
  ],
  declarations: [SigninEmailPage]
})
export class SigninEmailPageModule {}
