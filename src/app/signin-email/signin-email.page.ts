import { LoadingService } from './../services/loading/loading.service';
import firebase from "firebase/app";
import "firebase/auth";
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from "@angular/router";

@Component({
  selector: 'app-signin-email',
  templateUrl: './signin-email.page.html',
  styleUrls: ['./signin-email.page.scss'],
})
export class SigninEmailPage implements OnInit {
  signUpFormGroup: FormGroup;
  passwordType: any;

  constructor(private fb: FormBuilder,
    private router: Router,
    private loading: LoadingService,
    private toastController: ToastController) { }

  ngOnInit() {
    this.initiForms();
  }

  initiForms() {
    this.signUpFormGroup = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    })
  }
  togglePasswordType() {
    this.passwordType = this.passwordType || 'password';
    this.passwordType = (this.passwordType === 'password') ? 'text' : 'password';
  }

  Login() {
    this.loading.present('Signing you in...')
    firebase.auth().signInWithEmailAndPassword(this.signUpFormGroup.controls.email.value, this.signUpFormGroup.controls.password.value)
      .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        console.log('my user', user);
        this.presentToast("Login Successfull", "success");
        this.router.navigateByUrl('tabs', { replaceUrl: true });
        this.loading.dismiss();
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;

        this.presentToast("Login Unsuccessfull, please check your details", "danger");
        this.loading.dismiss();
        // ..
      });
  }







  async presentToast(message, color) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: color
    });
    toast.present();
  }
  get f(): any {
    return this.signUpFormGroup.controls;
  }
}
