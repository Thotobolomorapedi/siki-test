import { LoadingService } from './../services/loading/loading.service';
import firebase from 'firebase/app'
import 'firebase/auth'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoadingController, AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  forgotPasswordFormGroup: FormGroup;
  email: any;
  constructor(
    private fb: FormBuilder,
    public loading: LoadingService,
    private toastCtrl: ToastController,) { }

  ngOnInit() {
    this.initiForms();
  }
  initiForms() {
    this.forgotPasswordFormGroup = this.fb.group({
      email: ['', [Validators.email, Validators.required]]
    })
  }





resetPassword() {
  this.loading.present({
    message: 'Processing your credentials ...'
  })
      this.email = this.forgotPasswordFormGroup.controls.email.value
      firebase.auth().sendPasswordResetEmail(this.email).then(function () {
        // Email sent.

        this.presentToast("Password reset email has been sent, please check your inbox", "success")
        this.loading.dismiss();


      }).catch(function (error) {
        // An error happened.

        this.loading.dismiss();


      });
    }


  

  async presentToast(message, color) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      color: color
    });
    toast.present();
  }

  get f(): any {
    return this.forgotPasswordFormGroup.controls;
  }

}
