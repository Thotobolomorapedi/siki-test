import { DeclarationListEmitMode } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage  {
  public items: any[];
  public item1: any[];
  public item2: any[];
  public item3: any[];
  public item4: any[];
  public item5: any[];
  public item6: any[];
  public item7: any[];
  public item8: any[];
  public item9: any[];
  
  constructor() { 
    this.items =[
      {expanded: false},
      // {expannded: false},
    
    ],
    
    this.item1 =[
      {expanded: false},
      // {expannded: false},
    ];

    this.item2 =[
      {expanded: false},
      // {expannded: false},
    ];

    this.item3 =[
      {expanded: false},
      // {expannded: false},
    ];

    this.item4 =[
      {expanded: false},
      // {expannded: false},
    ];

    this.item5 =[
      {expanded: false},
      // {expannded: false},
    ];

    this.item6 =[
      {expanded: false},
      // {expannded: false},
    ];

    this.item7 =[
      {expanded: false},
      // {expannded: false},
    ];

    this.item8 =[
      {expanded: false},
      // {expannded: false},
    ];

    this.item9 =[
      {expanded: false},
      // {expannded: false},
    ];

  }

  expandItem(item): void{
    if (item.expanded) {
      item.expanded =false;
    } else{
      this.items.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item1.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item2.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });
      this.item3.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item4.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item5.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item6.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item7.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item8.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item9.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

    }
  }

}
