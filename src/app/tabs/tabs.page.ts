import { PaymentPage } from './../payment/payment.page';
import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { NavController, ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { SigninPage } from '../signin/signin.page';
import { Page404Page } from '../page404/page404.page';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  qrErroMessage: any = "The scanned code does not match RoutePay's code format. ";
  scannedCode: any;
  JsonValue: boolean;
  profileSet = false;
  paymentMethodSet: boolean;

  constructor(
    private modalCtrl: ModalController,
    private router: Router,
    private barcodeScanner: BarcodeScanner,
    private navCtrl: NavController,
    private storage: Storage) {
    this.storage.get('paymentOptions').then((val) => {
      console.log('Payment option not set', val);
      if (val === null) {
        this.paymentMethodSet = false;
      }
      else {
        this.paymentMethodSet = true;
      }
    });
    setTimeout(() => {
      this.lockApp();
    }, 5000);

  }
  async lockApp() {
    const modal = await this.modalCtrl.create({
      component: SigninPage,
      backdropDismiss: false,
      cssClass: 'login-modal'

    })
  }

  async errorPage() {
    const modal = await this.modalCtrl.create({
      component: Page404Page,
      componentProps: {
        'qrErrorMessage': this.qrErroMessage,
      },
      backdropDismiss: false,
      cssClass: 'error-modal'

    });
    return await modal.present();
  }

  IsJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  scan() {
    if (this.paymentMethodSet) {
      const Options = {
        preferFrontCamera: false, // iOS and Android
        showFlipCameraButton: false, // iOS and Android
        showTorchButton: false, // iOS and Android
        torchOn: false, // Android, launch with the torch switched on (if available)
        saveHistory: false, // Android, save scan history (default false)
        prompt: "Scan to Pay", // Android
        resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
        formats: "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
        orientation: "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
        disableAnimations: true, // iOS
        disableSuccessBeep: false // iOS and Android
      }
      this.barcodeScanner.scan(Options).then(barcodeData => {
        console.log("dt", barcodeData);
        this.JsonValue = this.IsJsonString(barcodeData.text);
        if (this.JsonValue) {
          this.scannedCode = JSON.parse(barcodeData.text);
          console.log("scannedCode", this.scannedCode);
          if (!this.scannedCode.merchantId) {
            this.errorPage();
          }
          else {
            const navigationExtras: NavigationExtras = {
              state: {
                appId: this.scannedCode.merchantId.appId,
                apiKey: this.scannedCode.merchantId.apiKey,
                secretToken: this.scannedCode.merchantId.secretToken,
              }
            };
            this.navCtrl.navigateForward(['payment'], navigationExtras);
          }
        }
        else {
          this.errorPage();
        }

      }).catch(err => {
       console.log("cancelled")
      });
    }
    else {
      this.router.navigate(['tabs/tab2',]);
    }
  }
  goToPayment(id) {
    this.router.navigate(['/payment', id]);
  }
}
