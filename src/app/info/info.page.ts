import { Component, OnInit } from '@angular/core';
import { ModalController} from '@ionic/angular';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  constructor( private modalController: ModalController) {}

  ngOnInit() {}
 
  async closeModal(){
    await this.modalController.dismiss();
  }
}
