import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-payment-confirmation',
  templateUrl: './payment-confirmation.page.html',
  styleUrls: ['./payment-confirmation.page.scss'],
})
export class PaymentConfirmationPage implements OnInit {
  @Input() merchantName: string;
  @Input() transactionAmount: any;
  @Input() transactionDate: any;

  amount: any;
  merchant: any;
  date: Date;
  constructor(private modalCtrl: ModalController,) { }

  ngOnInit() {
    this.amount = this.transactionAmount;
    this.merchant = this.merchantName;
    this.date = this.transactionDate;
  }
  async closeModal() {
    await this.modalCtrl.dismiss();
  }

 

}
