import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { AddpaymentPage } from '../addpayment/addpayment.page';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  walletDetails: any;
  date: any;
  tab: any = 'All';
  slideOpts = {
    autoplay: false
  };
  transactions: { img: string; name: string; date: any; amount: number; transactionType: string; }[];
  paymentMethodSet: boolean;



  constructor(
    private modalController: ModalController,
    public popoverController: PopoverController,
    public actionSheetController: ActionSheetController,
    private router: Router,
    private storage: Storage) {

  }
  ngOnInit(): void {
    this.storage.get('paymentOptions').then((val) => {
      console.log('Payment option not set', val);
      if (val === null) {
        this.paymentMethodSet = false;
      }
      else {
        this.paymentMethodSet = true;
        this.walletDetails = val;
      }
    });
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Payment Options',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Smega',
        icon: 'card',
        handler: () => {
          console.log('Smega clicked');
          this.router.navigateByUrl('addpayment', {replaceUrl:true});
        }
      },
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  async openModal() {
    const modal = await this.modalController.create({
      component: AddpaymentPage

    });
    return await modal.present();
  }

  async closeModal() {
    await this.modalController.dismiss();
  }

}
