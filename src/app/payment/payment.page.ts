import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import { LoadingService } from './../services/loading/loading.service';
import { AuthenticatePage } from './../authenticate/authenticate.page';
import { Merchant } from './../entities/merchantId';
import { Payment } from './../entities/payment';
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { IonInput, NavController, ModalController, LoadingController, ToastController } from '@ionic/angular';
import { CurrencyPipe } from '@angular/common';
import { Customer } from '../entities/customer';
import { PaymentService } from '../services/payment.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { PaymentConfirmationPage } from '../payment-confirmation/payment-confirmation.page';
import { TextMaskModule } from 'angular2-text-mask';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
  providers: [CurrencyPipe]
})
export class PaymentPage implements OnInit {
  usAmount = 1;
  merchant: any;
  payment: Payment;
  merchantId: Merchant;
  pin: any;
  customer: Customer;
  dataReturned: any;
  responseData: any;
  merchantName: any;
  transactionMessage: any;
  transactionAmount: any;
  transactionDate: any;
  amountForm: FormGroup;
  amount = 0.0;
  formattedAmount = '0.00';
  id: any;
  sub: any;
  scannedData = null;
  paymentMethodSet: boolean;
  walletDetails: any;
  console: any;
  payerId: any;

  constructor(
    private toastCtrl: ToastController,
    private loading: LoadingService,
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private currencyPipe: CurrencyPipe,
    private paymentService: PaymentService,
    private storage: Storage) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.scannedData = this.router.getCurrentNavigation().extras.state;
      console.log("here", this.scannedData);
    });

    this.storage.get('payerID').then((val) => {
      console.log('pAYDETAILS', val);
      if (val === null) {
        this.paymentMethodSet = false;
        console.log("kkk")
      }
      else {
        this.paymentMethodSet = true;
        console.log("mmmo", val);
        this.payerId = val;
      }
    });

    this.payment = new Payment();
    this.payment.merchantId = new Merchant();
    this.payment.customer = new Customer();
    this.payment.gatewayId = "SMEGA";
    this.payment.merchantId.apiKey = this.scannedData.apiKey;
    this.payment.merchantId.appId = this.scannedData.appId;
    this.payment.merchantId.secretToken = this.scannedData.secretToken;
    this.payment.customer.amount = 0.00;
    this.payment.customer.payerId = "73001349";
    this.payment.customer.pin = "4827";
    this.usAmount = this.usAmount;
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
  }
  transformAmount(element: HTMLElement) {
    this.formattedAmount = this.currencyPipe.transform(this.amount, 'BWP');
  }
  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: AuthenticatePage,
      cssClass: 'my-custom-class'
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        if (this.dataReturned) {
          console.log('ss >>', this.dataReturned);
          this.pin = this.dataReturned;
          this.pay();
        }
      }
      else { console.log('Response', 'false'); }
    });
    return await modal.present();
  }
  checkAuthentication() {
    if (this.amount < 2) {
      this.presentToast('Transaction amount cannot be less than 2.00');
    }
    else if (this.amount > 20) {
      this.presentToast('Transaction amount cannot be more than 20.00');
    }
    else {
      this.presentModal();
    }
  }
  pay() {
    this.payment.customer.amount = this.amount;
    this.payment.customer.pin = this.pin;
    this.payment.customer.payerId = this.payerId;
    console.log('data before', this.payment);
    this.loading.present('Processing payment...'
    );

    this.paymentService.makePayment(this.payment).then(res => {
      this.responseData = JSON.parse(res.data);
      console.log('response', this.responseData);
      if (this.responseData.txnData.approved) {
        console.log(' Is response authorized', this.responseData.txnData.approved);
        this.storeTransactionData(this.responseData.txnData.receiverId.name,
          this.responseData.txnData.txnDate,
          this.responseData.txnData.amount, "");

        this.paymentSuccessModal();
      }
      else {
        this.presentToast("Oops Transaction Unauthorized, Try Again ");
      }
    }).catch(error => {
      console.log(error.status);
      console.log(error.error);
      console.log(error.headers);
      this.presentToast("Failed to connect, Please try Again");
      console.log('server error', error);
    });
  }
  async presentToast(messageTxt) {
    const toast = this.toastCtrl.create({
      message: messageTxt,
      position: 'bottom',
      duration: 2000
    });
    (await toast).present();
  }
  storeTransactionData(mecharnt, date, amount, reference) {
    var user = firebase.auth().currentUser.uid;
    var rootRef = firebase.database().ref('transactions/' + user);
    console.log("user", user);
    console.log("database", rootRef);
    rootRef.push({
      mecharnt: mecharnt,
      date: date,
      amount: amount,
      reference: reference
    }).then(res => {
      this.loading.dismiss();
    }).catch(function (error) {
      // An error happened.
      this.loading.dismiss();
      console.log("store failed")
    });
  }
  async paymentSuccessModal() {
    const modal = await this.modalCtrl.create({
      component: PaymentConfirmationPage,
      componentProps: {
        'merchantName': this.responseData.txnData.receiverId.name,
        'transactionAmount': this.responseData.txnData.amount,
        'transactionDate': this.responseData.txnData.txnDate,
      },
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
