import { Injectable } from "@angular/core";
@Injectable()
export class ServiceEndPoints {
    constructor() {}
    public static readonly baseUrl: string = 'https://smegaapi-test.btc.bw/api/transact/jsonTxn';
}