import { Router } from '@angular/router';
import firebase from 'firebase/app'
import 'firebase/auth'
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TermsPage } from '../terms/terms.page';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  constructor(private router:Router, public modalController: ModalController ) { }

  ngOnInit() {
  }


  signout(){
    firebase.auth().signOut().then(() => {
      this.router.navigateByUrl('/first-time', { replaceUrl: true });
    }).catch((error) => {
      // An error happened.
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: TermsPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }



}
