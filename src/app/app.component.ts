import firebase from 'firebase/app';
import 'firebase/auth'
import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { firebaseConfig } from './credentials';
import { Router } from '@angular/router';



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  activePath = '';

  pages = [
    {
      name: 'Dashboard',
      path: '/menu/login'
      
    },
    {
      name: 'Register',
      path: '/menu/register'
    },
    {
      name: 'Home',
      path: '/menu/home'
    },
    {
      name: 'Security',
      path: '/menu/contact'
     } ,
  
     {
      name: 'Sign-Out',
      path: '/tabs/tab2'
     } ,

  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    private route: Router
  
  ) {
    firebase.initializeApp(firebaseConfig);
    this.initializeApp();

  }
  initializeApp() {
    this.platform.ready().then(() => {
      firebase.auth().onAuthStateChanged((user)=>{
        if(user){
          this.statusBar.styleDefault();
          this.splashScreen.hide();
          this.navCtrl.navigateRoot('tabs').then(()=>{  
         })
        }
        else{
          this.statusBar.styleDefault();
          this.splashScreen.hide();
          this.navCtrl.navigateRoot('welcome')
        }
      })
    });
  }
}
