import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  constructor( private modalController: ModalController,  private router: Router  ) { }

  ngOnInit() {
  }

  async closeModal() {
    await this.modalController.dismiss();
  }


  topup() {
    this.closeModal();
    this.router.navigateByUrl('/tabs');
  }

}
