import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { Platform, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
export class LockService {
    private onPauseSubscription: Subscription;
    private onResumeSubscription: Subscription;
    private lockScreen: any;
    private initialized = false;
    private isLocked = false;
    constructor(
        private paltform: Platform,
        private fiao: FingerprintAIO,
        private modalCtrl: ModalController,
        private splashScreen: SplashScreen
    ) { }

    init() {
        if (this.initialized) {
            return;
        }
    }

}