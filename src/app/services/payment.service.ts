import { HTTP } from '@ionic-native/http/ngx';
import { ServiceEndPoints } from './../ServiceEndpoints/service-endpoints';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Payment } from '../entities/payment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(public http: HTTP, public serviceEndPoints: ServiceEndPoints) {
  }

  makePayment(payment: Payment) {
    let headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    this.http.setDataSerializer('json')
    return this.http.post(ServiceEndPoints.baseUrl, payment, headers);
  }
}
