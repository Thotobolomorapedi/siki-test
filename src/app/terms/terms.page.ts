import { Component, OnInit } from '@angular/core';
import { ModalController} from '@ionic/angular';


@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
})
export class TermsPage {

  public items: any[];
  public item1: any[];

  constructor( public modalController: ModalController) {
    this.items =[
      {expanded: false},
      // {expannded: false},
    
    ],

    this.item1 =[
      {expanded: false},
      // {expannded: false},
    ];
  }

  

  // ngOnInit() {
  // }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

  expandItem(item): void{
    if (item.expanded) {
      item.expanded =false;
    } else{
      this.items.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });

      this.item1.map(listItem => {
        if(item == listItem) {
          listItem.expanded =!listItem.expanded;
        } else {
          listItem.expanded =false;
        }
        return listItem;
      });
    }
  }


}

