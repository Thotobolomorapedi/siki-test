import firebase from 'firebase/app'
import 'firebase/auth'
import { Component, OnInit } from '@angular/core';
import { NG_ASYNC_VALIDATORS } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import { LoadingService } from './../services/loading/loading.service';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  date: any;
  tab: any = 'All';
  profileSet = false;
  transactions: { img: string; name: string; date: any; amount: number; transactionType: string; }[];
  slideOpts = {
    initialSlide: 1,
    speed: 100,
    autoplay: true,
    loop: true
  };
  
  displayName: string;
  transactionsDB: any;
  sortedTransactions: any;
  constructor(private modalController: ModalController, public popoverController: PopoverController, private router: Router) 
  {
    this.date = Date.now();
    this.transactionsDB = [];
    this.sortedTransactions = []
  }
  ngOnInit() {
    var user = firebase.auth().currentUser;
    if (user.displayName === null) {
      this.profileSet = false;
      console.log("1111")
    }
    else {
      this.profileSet = true;
      this.displayName = user.displayName
      console.log("222")
      console.log("222", user)
    }
    this.getAllTransactions();
  }
  goToProfile() {
    this.router.navigateByUrl('/tabs');
  }
  getAllTransactions() {
    var user = firebase.auth().currentUser.uid;
    var rootRef = firebase.database().ref('transactions/' + user)
    // this.loading.present("Just a moment...")
    rootRef.get().then(snapshot => {
      if (snapshot.exists()) {
        snapshot.forEach(snapshot => {
          this.transactionsDB.push(snapshot.val())
        });
        this.transactionsDB.reverse();
        // this.loading.dismiss()
      }
      else {
        console.log("No data available");
      }
    }).catch(function (error) {
      console.error(error);
    });
  }
}
