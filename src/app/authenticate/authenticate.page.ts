import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';


@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.page.html',
  styleUrls: ['./authenticate.page.scss'],
})
export class AuthenticatePage implements OnInit {

  passcode: any;
  pageStatus: any;
  codeone: any;
  codetwo: any;
  codethree: any;
  codefour: any;
  int: any;
  newPincount: any;
  message: any;
  finalPin: any;
  fingerPin: any;
  accessGranted = false;
  constructor(
    public faio: FingerprintAIO,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    private modalController: ModalController,
    private storage: Storage) {
    this.passcode = '';
    this.finalPin = '';
    this.message = true;
    this.pageStatus = 'Enter Pin';
    this.int = 0;
    this.newPincount = 0;
    this.fingerPin = false;
  }
  ngOnInit() { }


  add(value) {
    console.log("LENGTH",  this.passcode.length)
    if (this.passcode.length < 4) {
      this.passcode = this.passcode + value;
      if (this.int === 0) {
        this.codeone = value;
        this.int++;
      } else if (this.int === 1) {
        this.codetwo = value;
        this.int++;
      } else if (this.int === 2) {
        this.codethree = value;
        this.int++;
      } else if (this.int === 3) {
        this.codefour = value;
        this.int++;
      }
      if (this.passcode.length === 4) {
        this.finalPin = this.codeone + this.codetwo + this.codethree + this.codefour
        console.log("finalPin", this.finalPin)
        this.closeModal();
      }
    }

  }

  delete() {
    if (this.passcode.length > 0) {
      if (this.passcode.length === 1) {
        this.codeone = null;
        this.int--;
      } else if (this.passcode.length === 2) {
        this.codetwo = null;
        this.int--;
      } else if (this.passcode.length === 3) {
        this.codethree = null;
        this.int--;
      } else if (this.passcode.length === 4) {
        this.codefour = null;
        this.int--;
      }
      this.passcode = this.passcode.substr(0, this.passcode.length - 1);
    }
  }


  async closeModal() {
    const onClosedData: any = this.finalPin;
    await this.modalController.dismiss(onClosedData);
  }
}
