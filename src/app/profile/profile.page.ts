import { LoadingService } from './../services/loading/loading.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import firebase from 'firebase/app'
import 'firebase/auth'
import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  profileFormGroup: FormGroup;
  fullName: any;
  profileSet = false
  phoneNumber: any;
  email: any;
  NgZone: any;
  constructor(private fb: FormBuilder,
    private router: Router,
    private loading: LoadingService,
    private toastController: ToastController,
    private zone: NgZone) {

  }

  ngOnInit() {
    var user = firebase.auth().currentUser;
    if (user.displayName === null) {
      this.profileSet = false;
      this.fullName = "Not Set";
      this.email = user.email ? user.email : "Not Set";
      this.phoneNumber = user.phoneNumber

    }
    else {
      var user = firebase.auth().currentUser;
      this.profileSet = true;
      this.fullName = user.displayName;
      this.email = user.email;
      this.phoneNumber = user.phoneNumber

    }


  }

  initiForm() {
    var user = firebase.auth().currentUser;
    this.profileFormGroup = this.fb.group({
      fullName: ['', [Validators.required]],
      email: user.email,
      phoneNumber: ['', Validators.required]
    })
  }

  updateProfile() {
    this.loading.present('updating your profile ...')
    var user = firebase.auth().currentUser;
    user.updateProfile({
      displayName: this.fullName,
     // photoURL: "https://example.com/jane-q-user/profile.jpg",
    }).then(function () {
      this.loading.dismiss();
      this.router.navigateByUrl('/tabs/tabs1', { replaceUrl: true });
      console.log("updated");

    }).catch(function (error) {
      this.loading.dismiss();
      console.log("update failed")
    });
  }

  signout() {
    this.loading.present('logging you out...')
    this.loading.dismiss();
    setTimeout( () => {
     
    }, 2000);

    firebase.auth().signOut().then(() => {
      this.router.navigateByUrl('/first-time', { replaceUrl: true });
    }).catch((error) => {
      this.loading.dismiss();
    });
  }
  async presentToast(message, color) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: color
    });
    toast.present();
  }


}
