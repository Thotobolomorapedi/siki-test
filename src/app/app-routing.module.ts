import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'welcome',
    loadChildren: () => import('src/app/welcome/welcome.module').then( m => m.WelcomePageModule)
  },
   {
    path: 'tabs',
    loadChildren: () => import('src/app/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('src/app/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'authenticate',
    loadChildren: () => import('src/app/authenticate/authenticate.module').then( m => m.AuthenticatePageModule)
  },
  {
    path: 'payment-confirmation',
    loadChildren: () => import('src/app/payment-confirmation/payment-confirmation.module').
    then( m => m.PaymentConfirmationPageModule)
  },
  {
    path: 'registration',
    loadChildren: () => import('src/app/registration/registration.module').then( m => m.RegistrationPageModule)
  },
  {
    path: 'first-time',
    loadChildren: () => import('src/app/first-time/first-time.module').then( m => m.FirstTimePageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('src/app/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('src/app/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'page404',
    loadChildren: () => import('src/app/page404/page404.module').then( m => m.Page404PageModule)
  },
  {
    path: 'security',
    loadChildren: () => import('src/app/security/security.module').then( m => m.SecurityPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('src/app/notifications/notifications.module').then( m => m.NotificationsPageModule)
  },

  {
    path: 'info',
    loadChildren: () => import('src/app/info/info.module').then( m => m.InfoPageModule)
  },

  {
    path: 'contact-us',
    loadChildren: () => import('src/app/contact-us/contact-us.module').then( m => m.ContactUsPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('src/app/faq/faq.module').then( m => m.FaqPageModule)
  },
  
  {
    path: 'addpayment',
    loadChildren: () => import('src/app/addpayment/addpayment.module').then( m => m.AddpaymentPageModule)
  },
 
 
  {
    path: 'signin',
    loadChildren: () => import('src/app/signin/signin.module').then( m => m.SigninPageModule)
  },
  
  {
    path: 'signin-email',
    loadChildren: () => import('src/app/signin-email/signin-email.module').then( m => m.SigninEmailPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('src/app/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'registered',
    loadChildren: () => import('./registered/registered.module').then( m => m.RegisteredPageModule)
  },
  {
    path: 'terms',
    loadChildren: () => import('./terms/terms.module').then( m => m.TermsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
