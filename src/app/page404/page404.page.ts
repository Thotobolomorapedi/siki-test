import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-page404',
  templateUrl: './page404.page.html',
  styleUrls: ['./page404.page.scss'],
})
export class Page404Page implements OnInit {
  @Input() qrErrorMessage: any;
  errorMessage: any;
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
    this.errorMessage = this.qrErrorMessage;
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }
}
