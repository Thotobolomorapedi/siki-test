import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { CurrencyPipe } from '@angular/common';
import { AppInfoPipe } from './app-info.pipe';
import { AngularFireModule } from '@angular/fire'
import { AngularFireAuthModule } from '@angular/fire/auth'
import { firebaseConfig } from './credentials';
import { ServiceEndPoints } from './ServiceEndpoints/service-endpoints';


@NgModule({
  declarations: [AppComponent, AppInfoPipe],
  entryComponents: [],
  imports:
    [
      AppRoutingModule,
      BrowserModule, IonicModule.forRoot(),
      IonicStorageModule.forRoot(),
      HttpClientModule,
      AngularFireAuthModule,
      AngularFireModule.initializeApp(firebaseConfig),
      IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    ServiceEndPoints,
    FingerprintAIO,
    HTTP,
    CurrencyPipe,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],

})
export class AppModule { }
